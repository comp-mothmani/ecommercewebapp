package com.nogroup.ecommerce.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.nogroup.ecommerce.data.entities.Category;

public class CategoryC extends ArrayList<Category> implements Serializable {

	/**
	 * @author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;

	public CategoryC() {
		
	}
	
	public CategoryC(Collection<Category> categories) {
		this.addAll(categories);
	}
}
