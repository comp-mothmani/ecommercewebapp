package com.nogroup.ecommerce.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.nogroup.ecommerce.data.entities.SubCategory;

public class SubCategoryC extends ArrayList<SubCategory> implements Serializable {

	/**
	 * @author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;

	public SubCategoryC() {
		
	}
	
	public SubCategoryC(Collection<SubCategory> subCategories) {
		this.addAll(subCategories);
	}
}
