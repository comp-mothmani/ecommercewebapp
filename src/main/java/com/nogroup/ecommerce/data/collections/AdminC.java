package com.nogroup.ecommerce.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.nogroup.ecommerce.data.entities.Admin;
import com.nogroup.ecommerce.data.entities.Category;
import com.nogroup.ecommerce.data.utils.PasswordUtils;

public class AdminC  extends ArrayList<Admin> implements Serializable {

	/**
	 * @author qosdesign
	 */
	private static final long serialVersionUID = 1L;
	
	public AdminC() {
		
	}
	
	public AdminC(Collection<Admin> admins) {
		this.addAll(admins);
	}

	public boolean checkAdmin(String email, String pass) {
		
		for (Admin tmp : this) {
	        String mySecurePassword = PasswordUtils.generateSecurePassword(pass, tmp.getSalt());

			if(tmp.getEmail().equals(email) &&  mySecurePassword.equals(tmp.getPassword())) {
				return true;
			}
		}
		return false;
		
	}

}
