package com.nogroup.ecommerce.data.collections;

import java.util.ArrayList;
import java.util.Collection;

import com.nogroup.ecommerce.data.entities.Purchase;

public class PurchaseC extends ArrayList<Purchase> {
	/**
	 * @author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;
	
	public PurchaseC() {
		
	}
	
	public PurchaseC(Collection<Purchase> purchases) {
		this.addAll(purchases);
	}

	public PurchaseC findById(int id) {
		PurchaseC tmps = new PurchaseC();
		for (Purchase tmp : this) {
			if (tmp.getUser().getId() == id) {
				tmps.add(tmp);
			}
		}
		return  tmps;
	}
}
