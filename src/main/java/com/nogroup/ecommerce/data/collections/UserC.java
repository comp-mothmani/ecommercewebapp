package com.nogroup.ecommerce.data.collections;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.nogroup.ecommerce.data.entities.User;
import com.nogroup.ecommerce.data.utils.PasswordUtils;

public class UserC  extends ArrayList<User> implements Serializable {

	/**
	 * @author qosdesign
	 */
	private static final long serialVersionUID = 1L;
	
	public UserC() {
		
	}
	
	public UserC(Collection<User> users) {
		this.addAll(users);
	}

	public boolean checkUser(String email, String pass) {
		
		for (User tmp : this) {
	        String mySecurePassword = PasswordUtils.generateSecurePassword(pass, tmp.getSalt());

			if(tmp.getEmail().equals(email) &&  mySecurePassword.equals(tmp.getPassword())) {
				return true;
			}
		}
		return false;
		
	}

}
