package com.nogroup.ecommerce.data.entities;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ManyToAny;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.nogroup.ecommerce.views.workflow.embedded.HomeViewer.TreeBean;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "price", "description", "pic", "availability" })
@Entity
@Table(name = "SUB_CATEGORY")
public class SubCategory implements TreeBean {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
	@JsonIgnore
	private Integer id;
	
	@JsonProperty("name")
    @Column(name = "name")
	private String name;
	
	@JsonProperty("price")
    @Column(name = "price")
	private String price;
	
	@JsonProperty("description")
    @Column(name = "description")
	private String description;
	
	@JsonProperty("pic")
    @Column(name = "pic", length = 50000)
	private String pic;
	
	@JsonProperty("availability")
    @Column(name = "availability")
	private Boolean availability;
	
    @JsonIgnore
    @JoinColumn(name = "category", nullable = false)
    @ManyToOne
	private Category category;
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("price")
	public String getPrice() {
		return price;
	}

	@JsonProperty("price")
	public void setPrice(String price) {
		this.price = price + " TND";
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("pic")
	public String getPic() {
		return pic;
	}

	@JsonProperty("pic")
	public void setPic(String pic) {
		this.pic = pic;
	}

	@JsonProperty("availability")
	public Boolean getAvailability() {
		return availability;
	}

	@JsonProperty("availability")
	public void setAvailability(Boolean availability) {
		this.availability = availability;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Override
	public String fetchName() {
		return getName();
	}

	@Override
	public String fetchPrice() {
		return getPrice();
	}

	@Override
	public String fetchDescription() {
		return getDescription();
	}

	@Override
	public String fetchAvailibility() {
		if(getAvailability()) {
			return "available";
		}
		return "not available";
	}
	
	

}