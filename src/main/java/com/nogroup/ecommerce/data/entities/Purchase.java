package com.nogroup.ecommerce.data.entities;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.nogroup.ecommerce.views.workflow.embedded.HomeViewer.TreeBean;

@Entity
@Table(name = "PURCHASE")
public class Purchase  implements TreeBean,Serializable{

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
	private Integer id;
	
	@Column(name = "content", length = 50000)
	private String content;
	
	@Column(name = "date")
	private Date date;
	
	
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "user",nullable = false)
    private User user ;


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	@Override
	public String fetchName() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String fetchPrice() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String fetchDescription() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String fetchAvailibility() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public int fetchId() {
		// TODO Auto-generated method stub
		return 0;
	}


}
