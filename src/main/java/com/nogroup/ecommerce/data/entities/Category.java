package com.nogroup.ecommerce.data.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.nogroup.ecommerce.views.workflow.embedded.HomeViewer.TreeBean;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "name", "description", "pic", "subCategories" })
@Entity
@Table(name = "CATEGORY")
public class Category implements TreeBean,Serializable{

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
	@JsonIgnore
	private Integer id;
	
	@Column(name = "name")
	@JsonProperty("name")
	private String name;
	
	@Column(name = "description")
	@JsonProperty("description")
	private String description;
	
	@JsonProperty("pic")
    @Column(name = "pic", length = 50000)
	private String pic;
	
	@JsonProperty("subCategories")
    @OneToMany(mappedBy = "category", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<SubCategory> subCategories = new ArrayList<SubCategory>();
	
	@JsonIgnore
	@Transient
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("pic")
	public String getPic() {
		return pic;
	}

	@JsonProperty("pic")
	public void setPic(String pic) {
		this.pic = pic;
	}

	@JsonProperty("subCategories")
	public List<SubCategory> getSubCategories() {
		return subCategories;
	}

	@JsonProperty("subCategories")
	public void setSubCategories(List<SubCategory> subCategories) {
		this.subCategories = subCategories;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String fetchName() {
		return getName();
	}

	@Override
	public String fetchPrice() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String fetchDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String fetchAvailibility() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
     * Remove an subDimenstions.
     * 
     */
    public void removeSubCategory(SubCategory val) {
        this.subCategories.remove(val) ;
    }

	@Override
	public int fetchId() {
		// TODO Auto-generated method stub
		return 0;
	}

	

}