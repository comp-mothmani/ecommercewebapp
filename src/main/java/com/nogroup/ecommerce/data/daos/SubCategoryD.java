package com.nogroup.ecommerce.data.daos;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.nogroup.ecommerce.data.entities.SubCategory;
import com.nogroup.ecommerce.data.utils.HUtils;
import com.nogroup.ecommerce.data.utils.Logger;

public class SubCategoryD {

	private static final long serialVersionUID = 1L;

	public Integer create(SubCategory e) {
		/*
		 * Insert new SubCategory record in the database.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.save(e);

			tx.commit();
		} catch (Exception ex) {
			if (tx != null)
				tx.rollback();
			ex.printStackTrace();
		}
		Logger.print("<SubCategory> Successfully created " + e.toString());
		return e.getId();
	}

	public List<SubCategory> read() {
		/*
		 * Select all SubCategory records from the database.
		 */
		List<SubCategory> tmps = new ArrayList<SubCategory>();
		Session session = HUtils.getSessionFactory().getCurrentSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			tmps = session.createQuery("FROM SubCategory").list();

			tx.commit();
		} catch (Exception ex) {
			if (tx != null)
				tx.rollback();
			ex.printStackTrace();
		}
		Logger.print("<Admin> Found " + tmps.size());
		return tmps;
	}

	public Long count() {
		/*
		 * Return SubCategory record count.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession();
		Transaction tx = null;
		Long count = (long) 0;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("select count(*) from SubCategory");
			count = (Long) query.uniqueResult();

			tx.commit();
		} catch (Exception ex) {
			if (tx != null)
				tx.rollback();
			ex.printStackTrace();
		}
		Logger.print("<SubCategory> Found " + count);
		return count;
	}

	public void delete(Integer id) {
		/*
		 * Delete SubCategory record by id.
		 */
		SubCategory e = findByID(id);

		Session session = HUtils.getSessionFactory().getCurrentSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.delete(e);

			tx.commit();
		} catch (Exception ex) {
			if (tx != null)
				tx.rollback();
			ex.printStackTrace();
		}
		Logger.print("<SubCategory>Successfully deleted " + e.toString());
	}

	public SubCategory findByID(Integer id) {
		/*
		 * Find SubCategory by id.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			SubCategory e = (SubCategory) session.load(SubCategory.class, id);

			tx.commit();
			return e;
		} catch (Exception ex) {
			if (tx != null)
				tx.rollback();
			ex.printStackTrace();
		}

		return null;
	}

	public void deleteAll() {
		/*
		 * Purge all Admin records.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("DELETE FROM SubCategory ");
			query.executeUpdate();

			tx.commit();
		} catch (Exception ex) {
			if (tx != null)
				tx.rollback();
			ex.printStackTrace();
		}
		Logger.print("<SubCategory>Successfully deleted all records.");
	}

	public boolean checkName(String name) {
		/*
		 * Check if name exists.
		 */
		if (count() == 0) {
			return false;
		} else {
			for (SubCategory tmp : read()) {
				if (tmp.getName().equals(name)) {
					return true;
				}
			}
			Logger.print("<SubCategory>Successfully checked all records.");
			return false;
		}

	}

	public void hard_update(SubCategory e) {
		/*
		 * Update SubCategory record.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.update(e);

			tx.commit();
		} catch (Exception ex) {
			if (tx != null)
				tx.rollback();
			ex.printStackTrace();
		}
		Logger.print("<SubCategory>Successfully updated " + e.toString());
	}

	public void delete(SubCategory e) {
		/*
		 * Delete SubCategory record.
		 */
		Session session = HUtils.getSessionFactory().getCurrentSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.delete(e);

			tx.commit();
		} catch (Exception ex) {
			if (tx != null)
				tx.rollback();
			ex.printStackTrace();
		}
		Logger.print("<SubCategory>Successfully deleted " + e.toString());
	}

	public SubCategory findByName(String item) {
		/*
		 * Find SubCategory record by name.
		 */
		for (SubCategory tmp : read()) {
			if (tmp.getName().equals(item)) {
				return tmp;
			}
		}
		Logger.print("<SubCategory>Successfully searched all records.");
		return null;
	}

}
