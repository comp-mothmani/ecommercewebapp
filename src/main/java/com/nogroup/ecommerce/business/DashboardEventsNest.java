package com.nogroup.ecommerce.business;

public class DashboardEventsNest {

	public static final class UpdateTreeRequestedEvent {
		
		 private String update;
		 private String pic;
		 
		 public UpdateTreeRequestedEvent(String update) {
			 this.update = update;
	
		 }
		 public UpdateTreeRequestedEvent(String update, String pic) {
			 this.update = update;
			 this.pic = pic;
	
		 }
		public String getUpdate() {
			return update;
		}
		public void setUpdate(String update) {
			this.update = update;
		}
		public String getPic() {
			return pic;
		}
		public void setPic(String pic) {
			this.pic = pic;
		}
		
		
	}
}
