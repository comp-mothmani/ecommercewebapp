package com.nogroup.ecommerce.views.workflow.home;



import com.nogroup.ecommerce.MyUI;
import com.nogroup.ecommerce.business.DashboardEventBus;
import com.nogroup.ecommerce.views.ccmp.DashboardMenu;
import com.nogroup.ecommerce.views.ccmp.btn.ValoMenuItemButton;
import com.nogroup.ecommerce.views.ccmp.cntrs.TabContainer;
import com.nogroup.ecommerce.views.winds.ManageCategoryWin;
import com.nogroup.ecommerce.views.winds.ManageSubCategoryWin;
import com.nogroup.ecommerce.views.winds.ManageUsersWin;
import com.nogroup.ecommerce.views.winds.NewCategoryWin;
import com.nogroup.ecommerce.views.winds.NewSubCategoryWin;
import com.nogroup.ecommerce.views.workflow.embedded.HomeViewer;
import com.vaadin.contextmenu.ContextMenu;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.ComponentContainer;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

public class MainView  extends HorizontalLayout {
	/**
	 * @author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;
	
	private DashboardMenu menu;

	@SuppressWarnings("deprecation")
	public MainView() {

		DashboardEventBus.register(this);

		setSizeFull();
		addStyleName("mainview");
		setSpacing(false);

		menu = new DashboardMenu();
		addComponent(menu);

		VerticalLayout v = new VerticalLayout();
		v.setMargin(false);
		v.setSpacing(false);
		v.setSizeFull();

		MyUI.container = new TabContainer();
		MyUI.container.setWidth("100%");
		MyUI.container.setHeight("100%");
		v.addComponent(MyUI.container);

		ComponentContainer content = new CssLayout();
		content.addStyleName("view-content");
		content.setSizeFull();

		v.addComponent(content);
		v.setExpandRatio(content, 1.0f);
		addComponent(v);
		setExpandRatio(v, 1.0f);

		UI.getCurrent().setNavigator(new Navigator(UI.getCurrent(), content));
		UI.getCurrent().getNavigator().addView("HomeViewer", HomeViewer.class);

		UI.getCurrent().getNavigator().navigateTo("HomeViewer");

		
		ValoMenuItemButton mp = menu.addMenuItem("Categories", FontAwesome.MAP, "HomeViewer", HomeViewer.class);
		builCategoryContextMenu(mp);
		
		mp = menu.addMenuItem("Sub-Categories", FontAwesome.MAP, "HomeViewer", HomeViewer.class);
		builSubCategoryContextMenu(mp);
		
		
		mp = menu.addMenuItem("Users", FontAwesome.MAP, "HomeViewer", HomeViewer.class);
		buildUsersContextMenu(mp);
	}

	private void builCategoryContextMenu(ValoMenuItemButton btn) {
		ContextMenu contextMenu = new ContextMenu(btn, true);

		contextMenu.addItem("New category", e -> {
			MyUI.container.addWindow(new NewCategoryWin(), "Create Category");
		});
		contextMenu.addItem("Category Manager", e -> {
			MyUI.container.addWindow(new ManageCategoryWin(), "Manage Categories");
		});
				
	}
	
	private void builSubCategoryContextMenu(ValoMenuItemButton btn) {
		ContextMenu contextMenu = new ContextMenu(btn, true);

		contextMenu.addItem("New Sub-Category", e -> {
			MyUI.container.addWindow(new NewSubCategoryWin(), "Create Sub-Category");
		});
		contextMenu.addItem("Sub-Category Manager", e -> {
			MyUI.container.addWindow(new ManageSubCategoryWin(), "Create Sub-Category");
		});
			
	}
	
	private void buildUsersContextMenu(ValoMenuItemButton btn) {
		ContextMenu contextMenu = new ContextMenu(btn, true);

		contextMenu.addItem("Users Manager", e -> {
			MyUI.container.addWindow(new ManageUsersWin(), "Create Sub-Category");
		});
			
	}

}
