package com.nogroup.ecommerce.views.workflow.embedded;

import com.google.common.eventbus.Subscribe;
import com.nogroup.ecommerce.business.DashboardEventBus;
import com.nogroup.ecommerce.business.DashboardEventsNest.UpdateTreeRequestedEvent;
import com.nogroup.ecommerce.data.daos.CategoryD;
import com.nogroup.ecommerce.data.entities.Category;
import com.nogroup.ecommerce.data.entities.SubCategory;
import com.vaadin.data.TreeData;
import com.vaadin.data.provider.TreeDataProvider;
import com.vaadin.navigator.View;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TreeGrid;
import com.vaadin.ui.VerticalLayout;

public class HomeViewer extends VerticalLayout implements View {
	
	/**
	 * @author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;
	private TreeGrid<TreeBean> grid ;
	private TreeData<TreeBean> treeData = new TreeData<>();
	
	public HomeViewer() {
		
		
		setWidth("100%");
		setHeight("100%");
		
		DashboardEventBus.register(this);

		
		treeData.clear();
		for (Category tmp : new CategoryD().read() ) {
			treeData.addItem(null,  tmp);
			for(SubCategory  tmp1 : tmp.getSubCategories()) {
				treeData.addItem(tmp, tmp1);

			}
		}

		grid = new TreeGrid<TreeBean>() ;
		grid.addStyleName("v-treegrid-cell-content");
		grid.setWidth("100%");
		grid.setHeight("100%");
		grid.setHeaderVisible(true);
		TreeDataProvider<TreeBean> inMemoryDataProvider = new TreeDataProvider<>(treeData);
		grid.setDataProvider(inMemoryDataProvider);;

		grid.addColumn(TreeBean::fetchName).setCaption("Category/Sub-Category");
		grid.addColumn(TreeBean::fetchPrice).setCaption("Price");
		grid.addColumn(TreeBean::fetchDescription).setCaption("Description") ;
		grid.addColumn(TreeBean::fetchAvailibility).setCaption("Availability") ;

		addComponent(grid);
		setMargin(true);

		
	}
	
	 
    @Subscribe
    public void testEventBus(UpdateTreeRequestedEvent event) {
    	if(event.getUpdate().equals("UPDATE GRID")) {
    		treeData.clear();
    		for (Category tmp : new CategoryD().read() ) {
    			treeData.addItem(null,  tmp);
    			for(SubCategory  tmp1 : tmp.getSubCategories()) {
    				treeData.addItem(tmp, tmp1);

    			}
    		}
    		grid.getDataProvider().refreshAll();
    	}
    }

	
	public interface TreeBean {

		public String fetchName() ;
		public String fetchPrice() ;
		public String fetchDescription() ;
		public String fetchAvailibility() ;
		public int fetchId() ;


	}
	


}


