package com.nogroup.ecommerce.views.ccmp.cbx;

import java.util.Collection;
import java.util.List;

import com.nogroup.ecommerce.data.daos.CategoryD;
import com.nogroup.ecommerce.data.entities.Category;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ItemCaptionGenerator;

public class CategoryCbx
    extends ComboBox<Category>
    implements ItemCaptionGenerator<Category>
{


    public CategoryCbx() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
    }

    public CategoryCbx(Collection<Category> vals) {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE CATEGORY");
    }

    public CategoryCbx(String caption) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE CATEGORY");
    }

    public CategoryCbx(String caption, Collection<Category> vals) {
        this.setCaption(caption);
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItems(vals);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE CATEGORY");
    }

    public CategoryCbx fromDB() {
        this.addStyleName("tiny");
        this.setWidth("100%");
        this.setEmptySelectionAllowed(false);
        this.setItemCaptionGenerator(this);
        this.setDescription("CHOOSE CATEGORY");
        List<Category> cities = new CategoryD().read() ;
        this.setItems(cities);
        
        if(cities.size() != 0) {
        	this.setValue(cities.get(0));
        }
        return this;
    }

    @Override
    public String apply(Category item) {
        return item.getName();
    }

}