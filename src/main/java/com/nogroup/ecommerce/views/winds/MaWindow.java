package com.nogroup.ecommerce.views.winds;
import org.vaadin.dialogs.ConfirmDialog;

import com.nogroup.ecommerce.views.workflow.embedded.HomeViewer.TreeBean;
import com.vaadin.data.TreeData;
import com.vaadin.data.provider.TreeDataProvider;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.ItemCaptionGenerator;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Tree;
import com.vaadin.ui.Tree.ItemClickListener;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public abstract class MaWindow extends CWindow {

	
	/**
	 * @author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;
	protected Panel pane;
	private Panel pane1;
	private Tree tree;
	private MenuBar menu1;

	public MaWindow() {
		
		setResizable(true);
		addStyleName("v-window-outerheader");
		caption(caption_());
		if(!width_().equals("")) {
			setWidth(width_());
		}
		if(!height_().equals("")) {
			setHeight(height_());
		}
		
		MenuBar menu = new MenuBar() ;
		menu.addStyleNames(ValoTheme.MENUBAR_SMALL,ValoTheme.MENUBAR_BORDERLESS);
		menu.addItem("",FontAwesome.FLOPPY_O,new Command() {
			
			@Override
			public void menuSelected(MenuItem selectedItem) {
				saveAction() ;
				//MaWindow.this.close();
			}
		}) ;

		addCaptionComponent(menu);
		
		menu1 = new MenuBar() ;
		menu1.addStyleNames(ValoTheme.MENUBAR_SMALL,ValoTheme.MENUBAR_BORDERLESS);
		menu1.addItem("",FontAwesome.TRASH_O,new Command() {
			
			@Override
			public void menuSelected(MenuItem selectedItem) {
				
				ConfirmDialog.show(UI.getCurrent(), "Please Confirm:", "Are you really sure?",
		        "I am", "Cancel", new ConfirmDialog.Listener() {

		            public void onClose(ConfirmDialog dialog) {
		                if (dialog.isConfirmed()) {
		                	deleteAction() ;
		                } else {
		                }
		            }
		        });
			}
		}) ;

		addCaptionComponent(menu1);
		
		
		
		VerticalLayout vl = new VerticalLayout() ;
		vl.setHeight("100%");
		vl.setMargin(false);
		addContent(vl);
		
		HorizontalLayout hznl = new HorizontalLayout() ;
		vl.addComponent(hznl);
		hznl.setWidth("100%");
		hznl.setHeight("100%");
		
		VerticalLayout vl1 = new VerticalLayout() ;
		vl1.setHeight("100%");
		vl1.setMargin(false);
	
		pane = new Panel();
		pane.setHeight("90%");
		pane.addStyleName(ValoTheme.PANEL_BORDERLESS);
		vl1.addComponent(pane);
		hznl.addComponent(vl1);
		hznl.setExpandRatio(vl1, (float) 2);
		
		VerticalLayout vl2 = new VerticalLayout() ;
		vl2.setHeight("100%");
		vl2.setMargin(false);
		pane1 = new Panel("Details Window") ;
		pane1.addStyleName(ValoTheme.PANEL_BORDERLESS);
		pane1.setHeight("90%");
		vl2.addComponent(pane1);
		hznl.addComponent(vl2);
		hznl.setExpandRatio(vl2, 4);
		
	}
	
	public  void initTree(TreeData<TreeBean> treeData,ItemClickListener<TreeBean> listener) {
		// An initial planet tree
	    tree = new Tree<>();
		tree.setHeight("100%");
		tree.setWidth("90%");

		tree.setItemCaptionGenerator(new ItemCaptionGenerator<TreeBean>() {
			
			@Override
			public String apply(TreeBean item) {
				return item.fetchName();
			}
		});
		
		
		tree.addStyleName("v-captiontext");

		TreeDataProvider inMemoryDataProvider = new TreeDataProvider<>(treeData);
		tree.setDataProvider(inMemoryDataProvider);
		tree.addItemClickListener(listener) ;
		pane.setContent(tree);
		
	}
	
	
	public void clearDetails() {
		
	}

	public void setDetails(Component cmp) {
		pane1.setContent(cmp);
	}
	public abstract void switch2() ;
	public abstract void initData() ;
	public abstract String caption_() ;
	public abstract String width_() ;
	public abstract String height_() ;
	public abstract void saveAction() ;
	public abstract void deleteAction() ;
	
	public Panel getPane() {
		return pane;
	}

	public void setPane(Panel pane) {
		this.pane = pane;
	}

	public Tree getTree() {
		return tree;
	}

	public void setTree(Tree tree) {
		this.tree = tree;
	}

	public MenuBar getMenu1() {
		return menu1;
	}

	public void setMenu1(MenuBar menu1) {
		this.menu1 = menu1;
	}
	
	
	
}