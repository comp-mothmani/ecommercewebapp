package com.nogroup.ecommerce.views.winds;

import com.nogroup.ecommerce.business.DashboardEventBus;
import com.nogroup.ecommerce.business.DashboardEventsNest.UpdateTreeRequestedEvent;
import com.nogroup.ecommerce.data.daos.SubCategoryD;
import com.nogroup.ecommerce.data.entities.SubCategory;
import com.nogroup.ecommerce.views.ccmp.UploadExample;
import com.nogroup.ecommerce.views.ccmp.cbx.CategoryCbx;
import com.nogroup.ecommerce.views.ccmp.cntrs.CrWindow;
import com.vaadin.data.Binder;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class NewSubCategoryWin extends CrWindow  {
	
	/**
	 * Author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;
	private SubCategory entity = new SubCategory() ;
	private Label label;
	private Image image;
	private UploadExample upp;
	private CategoryCbx cbx;
	private ComboBox<String> cbxAvailibility;
	private TextField tf2;

	public NewSubCategoryWin() {
		
		DashboardEventBus.register(this);

		
		VerticalLayout vl = new VerticalLayout() ;
		vl.setSpacing(false);
		addContent(vl);
		
		Binder<SubCategory> binder = new Binder<>();
		binder.setBean(entity);
	   
		
		cbx = new CategoryCbx("Choose Category").fromDB();
		vl.addComponent(cbx);

		TextField tf = new TextField("SubCategory name");
		tf.setPlaceholder("SubCategory name");
		tf.setWidth("100%");
		tf.setSizeFull();
		tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
		binder.bind(tf, SubCategory::getName, SubCategory::setName);
		vl.addComponent(tf);
		
		
	    TextField tf1 = new TextField("Description") ;
		tf1.setPlaceholder("Description");
		tf1.setWidth("100%");
		tf1.addStyleName(ValoTheme.TEXTFIELD_TINY);
		binder.bind(tf1, SubCategory::getDescription, SubCategory::setDescription);
		vl.addComponent(tf1);
		

		 tf2 = new TextField("Price") ;
		tf2.setPlaceholder("Price");
		tf2.setWidth("100%");
		tf2.addStyleName(ValoTheme.TEXTFIELD_TINY);
		binder.bind(tf2, SubCategory::getPrice, SubCategory::setPrice);
		vl.addComponent(tf2);
		
		cbxAvailibility = new ComboBox<String>("Availibility");
		cbxAvailibility.setCaption("Availibility");
		cbxAvailibility.addStyleName("tiny");
		cbxAvailibility.setWidth("100%");
		cbxAvailibility.setEmptySelectionAllowed(false);
        cbxAvailibility.setDescription("Availibility");
        String[] array = {"available","not available"};
        cbxAvailibility.setItems(array);
		vl.addComponent(cbxAvailibility);

		upp = new UploadExample();
		upp.init("advanced");
		vl.addComponent(upp);

	}
	
	
	@Override
	public String caption_() {
		return "Add new sub-category";
	}

	@Override
	public String width_() {
		return "35%";
	}

	@Override
	public String height_() {
		return "45%";
	}

	@Override
	public void saveAction() {
		
		try {
			if (!tf2.getValue().matches("[0-9]+")){
				Notification.show("Price field accepts only digits", Notification.TYPE_ERROR_MESSAGE);
				return;
			}
			entity.setPic(upp.getBase64());
			entity.setCategory(cbx.getValue());
			if(cbxAvailibility.getValue().equals("available")) {
				entity.setAvailability(true);
			}else {
				entity.setAvailability(false);
			}
			new SubCategoryD().create(entity);
			DashboardEventBus.post(new UpdateTreeRequestedEvent("UPDATE GRID"));
			this.close();
			Notification.show("New sub-category created successfully");
		}catch (Exception e) {
			Notification.show("Failed to create new sub-category", Notification.TYPE_ERROR_MESSAGE);
		}
		
	}

	@Override
	public void windowClosed(CloseEvent e) {
		// TODO Auto-generated method stub
		
	}


}

