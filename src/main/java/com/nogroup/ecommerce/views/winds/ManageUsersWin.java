package com.nogroup.ecommerce.views.winds;

import java.io.IOException;
import java.util.Base64;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nogroup.ecommerce.data.collections.PurchaseC;
import com.nogroup.ecommerce.data.daos.PurchaseD;
import com.nogroup.ecommerce.data.daos.UserD;
import com.nogroup.ecommerce.data.entities.Purchase;
import com.nogroup.ecommerce.data.entities.User;
import com.nogroup.ecommerce.data.models.PurchaseTemp;
import com.nogroup.ecommerce.views.ccmp.UploadExample;
import com.nogroup.ecommerce.views.workflow.embedded.HomeViewer.TreeBean;
import com.vaadin.data.TreeData;
import com.vaadin.data.provider.TreeDataProvider;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.ItemCaptionGenerator;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Tree;
import com.vaadin.ui.Tree.ItemClick;
import com.vaadin.ui.Tree.ItemClickListener;
import com.vaadin.ui.VerticalLayout;

public class ManageUsersWin  extends MaWindow implements ItemClickListener<TreeBean> {

	/**
	 * @author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;
	private TreeData<TreeBean> treeData = new TreeData<>();
	private TreeBean selectedItem;
	private UploadExample upp;

	
	public ManageUsersWin() {
		customTree(treeData, this);
		getPane().setCaption("Manage users details");

	}
	public void customTree(TreeData<TreeBean> treeData, ItemClickListener<TreeBean> listener) {
		VerticalLayout vl = new VerticalLayout();
		vl.setWidth("100%");

		Tree tree = new Tree<>();
		vl.addComponent(tree);
		tree.setHeight("100%");
		tree.setWidth("90%");

		tree.setItemCaptionGenerator(new ItemCaptionGenerator<TreeBean>() {

			@Override
			public String apply(TreeBean item) {
				return item.fetchName();
			}
		});

		tree.addStyleName("v-captiontext");

		TreeDataProvider inMemoryDataProvider = new TreeDataProvider<>(treeData);
		tree.setDataProvider(inMemoryDataProvider);
		tree.addItemClickListener(listener);

		for (User tmp : new UserD().read()) {
			treeData.addItem(null, tmp);

		}
		
		pane.setContent(vl);
	}

	@Override
	public void itemClick(ItemClick<TreeBean> event) {
		selectedItem = event.getItem();
		switch2();		
	}

	@Override
	public void switch2() {
		setDetails(null);
		setDetails(new EmbeddedView());
		
	}

	@Override
	public void initData() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String caption_() {
		return "Manage Users";
	}

	@Override
	public String width_() {
		return "50%";
	}

	@Override
	public String height_() {
		return "65%";
	}
	

	@Override
	public void saveAction() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAction() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(CloseEvent e) {
		// TODO Auto-generated method stub
		
	}
	

	protected class EmbeddedView extends VerticalLayout {
		/**
		 * @author mahdiothmani
		 */
		private static final long serialVersionUID = 1L;

		public EmbeddedView() {
			if (selectedItem == null) {
				return;
			} else {
				new usersDetailsView(this);
			}
		}
	}

	protected class usersDetailsView {
		/**
		 * @author mahdiothmani
		 */
		private static final long serialVersionUID = 1L;

		public usersDetailsView(EmbeddedView root) {

			if(selectedItem == null) {
				Notification.show("ERROR NULL",
						Notification.Type.HUMANIZED_MESSAGE);
				return;
			}
						
			
			for (Purchase tmp : new PurchaseC(new PurchaseD().read()).findById(selectedItem.fetchId())) {
				PurchaseTemp temp = null;
				try {
					temp = new ObjectMapper().readValue(tmp.getContent(), PurchaseTemp.class);
				} catch (JsonParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JsonMappingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String ss = "<div class=\"row\">\n" + 
						"<div class=\"column\" >\n" + 
						"<h2>P_R_O_D_U_C_T_N_A_M_E</h2>\n" + 
						"<p>P_R_O_D_U_C_T_Q_U_A_N_T_I_T_Y</p>\n" + 
						"<p>P_R_O_D_U_C_T_P_R_I_C_E</p>\n" + 
						"<p>P_U_R_C_H_A_S_E_D_A_T_E</p>\n" + 
						"</div>\n" + 
						"</div>\n" + 
						"<style>\n" + 
						"div.column {\n" + 
						"width: 100%\n" + 
						"}\n" + 
						"</style>";
				ss = ss.replace("P_R_O_D_U_C_T_N_A_M_E", "Product: " + temp.getProductName());
				ss = ss.replace("P_R_O_D_U_C_T_Q_U_A_N_T_I_T_Y", "Quantity: " + temp.getProductQuantity());
				ss = ss.replace("P_R_O_D_U_C_T_P_R_I_C_E", "Price: " +temp.getProductPrice());
				ss = ss.replace("P_U_R_C_H_A_S_E_D_A_T_E", "Date: " + tmp.getDate().toString());

				Label htmlLabel = new Label(ss,
					    ContentMode.HTML);
				
				
				root.addComponent(htmlLabel);
			}

		}

	}

	public byte[] decoder(String img) {
		byte[] bytes = Base64.getDecoder().decode(img);
		return bytes;
	}


}
