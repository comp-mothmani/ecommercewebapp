package com.nogroup.ecommerce.views.winds;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Base64;

import com.nogroup.ecommerce.ConstantsEC;
import com.nogroup.ecommerce.business.DashboardEventBus;
import com.nogroup.ecommerce.business.DashboardEventsNest.UpdateTreeRequestedEvent;
import com.nogroup.ecommerce.data.daos.CategoryD;
import com.nogroup.ecommerce.data.entities.Category;
import com.nogroup.ecommerce.views.ccmp.UploadExample;
import com.nogroup.ecommerce.views.workflow.embedded.HomeViewer.TreeBean;
import com.vaadin.data.Binder;
import com.vaadin.data.TreeData;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Tree.ItemClick;
import com.vaadin.ui.Tree.ItemClickListener;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class ManageCategoryWin extends MaWindow implements ItemClickListener<TreeBean>{

	
	/**
	 * @author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;
	private TreeData<TreeBean> treeData = new TreeData<>();
	private TreeBean selectedItem;
	private Category entity = new Category();
	private UploadExample upp;

	
	public  ManageCategoryWin() {
		initData();
		initTree(treeData,this) ;
		getPane().setCaption("Manage category details");
	}

	@Override
	public void switch2() {	
		setDetails(null);
		setDetails(new EmbeddedView());
	}

	@Override
	public void initData() {
		treeData.clear();
		for (Category tmp : new CategoryD().read() ) {
				treeData.addItem(null, tmp);
		}	
	}

	@Override
	public String caption_() {
		return "Manage Categories";
	}

	@Override
	public String width_() {
		return "50%";
	}

	@Override
	public String height_() {
		return "65%";
	}

	@Override
	public void saveAction() {
		try {
			// picture
			System.out.println("PIC: " +ConstantsEC.UPLOADED_PICTURE );
			((Category) selectedItem).setPic(ConstantsEC.UPLOADED_PICTURE);

			new CategoryD().hard_update((Category) selectedItem);
			Notification.show(((Category) selectedItem).getName() + " successfully updated",
	                Notification.Type.HUMANIZED_MESSAGE);
			
			DashboardEventBus.post(new UpdateTreeRequestedEvent("UPDATE GRID"));

		}catch (Exception e) {
			Notification.show(entity.getName() + " Failed to update " + ((Category) selectedItem).getName(),
	                Notification.Type.ERROR_MESSAGE);		
			}
		
	}

	@Override
	public void deleteAction() {
		try{
			new CategoryD().delete(((Category) selectedItem).getId());
			setDetails(null);
			treeData.removeItem(selectedItem);
			Notification.show(((Category) selectedItem).getName()+" Update successfully removed",
	                Notification.Type.HUMANIZED_MESSAGE);
			getTree().getDataProvider().refreshAll();
			DashboardEventBus.post(new UpdateTreeRequestedEvent("UPDATE GRID"));
		}catch (Exception e) {
			Notification.show(entity.getName() + " Failed to update " + ((Category) selectedItem).getName(),
	                Notification.Type.ERROR_MESSAGE);		
		}
	}

	@Override
	public void windowClosed(CloseEvent e) {
	}

	@Override
	public void itemClick(ItemClick<TreeBean> event) {	
		selectedItem = event.getItem() ;
		switch2();
	}
	
	protected class EmbeddedView extends VerticalLayout{
		/**
		 * @author mahdiothmani
		 */
		private static final long serialVersionUID = 1L;
		
		public EmbeddedView() {
			if(selectedItem == null) {
				return ;
			}else {
				new CategoryDetailsView(this);
			}
		}		
	}
	
	protected class CategoryDetailsView {
		/**
		 * @author mahdiothmani
		 */
		private static final long serialVersionUID =  1L;
		public CategoryDetailsView(EmbeddedView root) {
			
			Binder<Category> binder = new Binder<>();
			binder.setBean((Category) selectedItem);

			TextField tf = new TextField("Category name");

			tf.setPlaceholder("Category name");
			tf.setWidth("100%");
			tf.setSizeFull();
			tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
			binder.bind(tf, Category::getName, Category::setName);
			root.addComponent(tf);
			
			
		    TextField tf1 = new TextField("Description") ;
			tf1.setPlaceholder("Description");
			tf1.setWidth("100%");
			tf1.addStyleName(ValoTheme.TEXTFIELD_TINY);
			binder.bind(tf1, Category::getDescription, Category::setDescription);
			root.addComponent(tf1);

			
			upp = new UploadExample();
			upp.init("advanced");
			
			if((((Category)selectedItem).getPic() != "")) {
				StreamSource source = new StreamSource() {
                    private static final long serialVersionUID = -4905654404647215809L;

                    public InputStream getStream() {
                        return new ByteArrayInputStream(decoder((((Category)selectedItem).getPic())));
                    }
                };
                
                upp.getImageAdvanced().setSource(new StreamResource(source,""));
                upp.getImageAdvanced().setVisible(true);
                upp.getImageAdvanced().setCaption("");
                upp.getImageAdvanced().setWidth("50%");
                upp.getImageAdvanced().setHeight("50%");
                upp.getImageAdvanced().markAsDirty();
			}
		
			root.addComponent(upp);
			

		}
		
	}
	
	public  byte[] decoder(String img) {
        byte[] bytes = Base64.getDecoder().decode(img);
        return bytes;
	}
	
	
	

}
