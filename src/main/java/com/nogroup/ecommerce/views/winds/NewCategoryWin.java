package com.nogroup.ecommerce.views.winds;


import com.nogroup.ecommerce.business.DashboardEventBus;
import com.nogroup.ecommerce.business.DashboardEventsNest.UpdateTreeRequestedEvent;
import com.nogroup.ecommerce.data.daos.CategoryD;
import com.nogroup.ecommerce.data.entities.Category;
import com.nogroup.ecommerce.views.ccmp.UploadExample;
import com.nogroup.ecommerce.views.ccmp.cntrs.CrWindow;
import com.vaadin.data.Binder;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class NewCategoryWin extends CrWindow  {
	
	/**
	 * Author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;
	private Category entity = new Category() ;
	private Label label;
	private Image image;
	private UploadExample upp;


	public NewCategoryWin() {
		
		DashboardEventBus.register(this);

		
		VerticalLayout vl = new VerticalLayout() ;
		vl.setSpacing(false);
		addContent(vl);
		
		Binder<Category> binder = new Binder<>();
		binder.setBean(entity);
	    
		/*img = new ImageUpload(entity) ;
        vl.addComponent(img);*/

		TextField tf = new TextField("Category name");
		tf.setPlaceholder("Category name");
		tf.setWidth("100%");
		tf.setSizeFull();
		tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
		binder.bind(tf, Category::getName, Category::setName);
		vl.addComponent(tf);
		
		
	    TextField tf1 = new TextField("Description") ;
		tf1.setPlaceholder("Description");
		tf1.setWidth("100%");
		tf1.addStyleName(ValoTheme.TEXTFIELD_TINY);
		binder.bind(tf1, Category::getDescription, Category::setDescription);
		vl.addComponent(tf1);

		
		upp = new UploadExample();
		upp.init("advanced");
		vl.addComponent(upp);

	}
	
	
	@Override
	public String caption_() {
		return "Add new category";
	}

	@Override
	public String width_() {
		return "35%";
	}

	@Override
	public String height_() {
		return "45%";
	}

	@Override
	public void saveAction() {
		try {
			entity.setPic(upp.getBase64());
			new CategoryD().create(entity);
			DashboardEventBus.post(new UpdateTreeRequestedEvent("UPDATE GRID"));
			this.close();
			Notification.show("New category created successfully");
		}catch (Exception e) {
			Notification.show("Failed to create new category", Notification.TYPE_ERROR_MESSAGE);
		}
		
		
	}

	@Override
	public void windowClosed(CloseEvent e) {
		// TODO Auto-generated method stub
		
	}


}
