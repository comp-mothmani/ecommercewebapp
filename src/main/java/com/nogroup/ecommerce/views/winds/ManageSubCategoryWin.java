package com.nogroup.ecommerce.views.winds;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Base64;

import com.nogroup.ecommerce.ConstantsEC;
import com.nogroup.ecommerce.business.DashboardEventBus;
import com.nogroup.ecommerce.business.DashboardEventsNest.UpdateTreeRequestedEvent;
import com.nogroup.ecommerce.data.daos.CategoryD;
import com.nogroup.ecommerce.data.entities.Category;
import com.nogroup.ecommerce.data.entities.SubCategory;
import com.nogroup.ecommerce.views.ccmp.UploadExample;
import com.nogroup.ecommerce.views.ccmp.cbx.CategoryCbx;
import com.nogroup.ecommerce.views.workflow.embedded.HomeViewer.TreeBean;
import com.vaadin.data.Binder;
import com.vaadin.data.TreeData;
import com.vaadin.data.converter.StringToIntegerConverter;
import com.vaadin.data.provider.TreeDataProvider;
import com.vaadin.event.selection.SingleSelectionEvent;
import com.vaadin.event.selection.SingleSelectionListener;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.ItemCaptionGenerator;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Tree;
import com.vaadin.ui.Tree.ItemClick;
import com.vaadin.ui.Tree.ItemClickListener;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class ManageSubCategoryWin extends MaWindow implements ItemClickListener<TreeBean> {

	/**
	 * @author mahdiothmani
	 */
	private static final long serialVersionUID = 1L;
	private TreeData<TreeBean> treeData = new TreeData<>();
	private TreeBean selectedItem;
	private UploadExample upp;
	private CategoryCbx cbxCategory;
	private ComboBox<String> cbxAvailibility;
	private TextField tf2;
	private String base64;

	public ManageSubCategoryWin() {
		initData();
		customTree(treeData, this);
		getPane().setCaption("Manage category details");
	}

	public void customTree(TreeData<TreeBean> treeData, ItemClickListener<TreeBean> listener) {
		VerticalLayout vl = new VerticalLayout();
		vl.setWidth("100%");
		CategoryCbx cbx = new CategoryCbx("Filter by category").fromDB();
		vl.addComponent(cbx);

		Tree tree = new Tree<>();
		vl.addComponent(tree);
		tree.setHeight("100%");
		tree.setWidth("90%");

		tree.setItemCaptionGenerator(new ItemCaptionGenerator<TreeBean>() {

			@Override
			public String apply(TreeBean item) {
				return item.fetchName();
			}
		});

		tree.addStyleName("v-captiontext");

		TreeDataProvider inMemoryDataProvider = new TreeDataProvider<>(treeData);
		tree.setDataProvider(inMemoryDataProvider);
		tree.addItemClickListener(listener);

		cbx.addSelectionListener(new SingleSelectionListener<Category>() {

			@Override
			public void selectionChange(SingleSelectionEvent<Category> event) {
				Notification.show("Filter sub-categories with (" + event.getValue().getName() + ")");

				treeData.clear();
				for (Category tmp : new CategoryD().read()) {
					if ((tmp.getId() == event.getValue().getId())
							&& (tmp.getName().equals(event.getValue().getName()))) {
						treeData.clear();
						for (SubCategory tmp1 : tmp.getSubCategories()) {
							treeData.addItem(null, tmp1);
						}
					}
				}

				tree.getDataProvider().refreshAll();
			}
		});

		pane.setContent(vl);
	}

	@Override
	public void switch2() {
		setDetails(null);
		setDetails(new EmbeddedView());
	}

	@Override
	public void initData() {
		/*
		 * treeData.clear(); for (SubCategory tmp : new SubCategoryD().read() ) {
		 * treeData.addItem(null, tmp); }
		 */
	}

	@Override
	public String caption_() {
		return "Manage Sub-Categories";
	}

	@Override
	public String width_() {
		return "50%";
	}

	@Override
	public String height_() {
		return "65%";
	}
	

	@Override
	public void saveAction() {

		try {
			if (!tf2.getValue().matches("[0-9]+")) {
				Notification.show("Price field accepts only digits", Notification.TYPE_ERROR_MESSAGE);
				return;
			}
			// picture
			System.out.println("PIC: " +ConstantsEC.UPLOADED_PICTURE );
			((SubCategory) selectedItem).setPic(ConstantsEC.UPLOADED_PICTURE);

			((SubCategory) selectedItem).getCategory().getSubCategories().remove((SubCategory) selectedItem);
			((SubCategory) selectedItem).setCategory(cbxCategory.getValue());
			if (cbxAvailibility.getValue().equals("available")) {
				((SubCategory) selectedItem).setAvailability(true);
			} else {
				((SubCategory) selectedItem).setAvailability(false);
			}
			((SubCategory) selectedItem).getCategory().getSubCategories().add((SubCategory) selectedItem);
			new CategoryD().hard_update(((SubCategory) selectedItem).getCategory());
			DashboardEventBus.post(new UpdateTreeRequestedEvent("UPDATE GRID"));
			// this.close();
			Notification.show(((SubCategory) selectedItem).getName() + " updated successfully");
		} catch (Exception e) {
			Notification.show(((SubCategory) selectedItem).getName() + " failed to update",
					Notification.TYPE_ERROR_MESSAGE);
		}
	}

	@Override
	public void deleteAction() {

		if (selectedItem == null) {
			Notification.show("You must select an element");
			return;
		}

		try {

			Category category = ((SubCategory) selectedItem).getCategory();
			category.removeSubCategory((SubCategory) selectedItem);
			new CategoryD().hard_update(category);

			treeData.removeItem(selectedItem);
			setDetails(null);

			Notification.show(((SubCategory) selectedItem).getName() + " successfully removed",
					Notification.Type.HUMANIZED_MESSAGE);
			selectedItem = null;
			initData();
			customTree(treeData, this);
			DashboardEventBus.post(new UpdateTreeRequestedEvent("UPDATE GRID"));

		} catch (Exception e) {
			Notification.show("Failed to remove " + ((SubCategory) selectedItem).getName(),
					Notification.Type.ERROR_MESSAGE);

		}
	}

	@Override
	public void windowClosed(CloseEvent e) {
	}

	@Override
	public void itemClick(ItemClick<TreeBean> event) {
		selectedItem = event.getItem();
		switch2();
	}

	protected class EmbeddedView extends VerticalLayout {
		/**
		 * @author mahdiothmani
		 */
		private static final long serialVersionUID = 1L;

		public EmbeddedView() {
			if (selectedItem == null) {
				return;
			} else {
				new SubCategoryDetailsView(this);
			}
		}
	}

	protected class SubCategoryDetailsView {
		/**
		 * @author mahdiothmani
		 */
		private static final long serialVersionUID = 1L;

		public SubCategoryDetailsView(EmbeddedView root) {

			Binder<SubCategory> binder = new Binder<>();
			binder.setBean((SubCategory) selectedItem);

			cbxCategory = new CategoryCbx("Choose Category").fromDB();
			Category category = ((SubCategory) selectedItem).getCategory();
			cbxCategory.setValue(category);
			root.addComponent(cbxCategory);

			TextField tf = new TextField("SubCategory name");
			tf.setPlaceholder("SubCategory name");
			tf.setWidth("100%");
			tf.setSizeFull();
			tf.addStyleName(ValoTheme.TEXTFIELD_TINY);
			binder.bind(tf, SubCategory::getName, SubCategory::setName);
			root.addComponent(tf);

			TextField tf1 = new TextField("Description");
			tf1.setPlaceholder("Description");
			tf1.setWidth("100%");
			tf1.addStyleName(ValoTheme.TEXTFIELD_TINY);
			binder.bind(tf1, SubCategory::getDescription, SubCategory::setDescription);
			root.addComponent(tf1);

			tf2 = new TextField("Price");
			tf2.setPlaceholder("Price");
			tf2.setWidth("100%");
			tf2.addStyleName(ValoTheme.TEXTFIELD_TINY);
			binder.bind(tf2, SubCategory::getPrice, SubCategory::setPrice);
			binder.forField(tf).withConverter(new StringToIntegerConverter("Must be Integer"));
			root.addComponent(tf2);

			cbxAvailibility = new ComboBox<String>("Availibility");
			cbxAvailibility.setCaption("Availibility");
			cbxAvailibility.addStyleName("tiny");
			cbxAvailibility.setWidth("100%");
			cbxAvailibility.setEmptySelectionAllowed(false);
			cbxAvailibility.setDescription("Availibility");
			if (((SubCategory) selectedItem).getAvailability()) {
				cbxAvailibility.setValue("available");
			} else {
				cbxAvailibility.setValue("not available");
			}
			String[] array = { "available", "not available" };
			cbxAvailibility.setItems(array);
			root.addComponent(cbxAvailibility);

			upp = new UploadExample();
			upp.init("advanced");

			if ((((SubCategory) selectedItem).getPic() != "")) {
				StreamSource source = new StreamSource() {
					private static final long serialVersionUID = -4905654404647215809L;

					public InputStream getStream() {
						return new ByteArrayInputStream(decoder((((SubCategory) selectedItem).getPic())));
					}
				};

				upp.getImageAdvanced().setSource(new StreamResource(source, ""));
				upp.getImageAdvanced().setVisible(true);
				upp.getImageAdvanced().setCaption("");
				upp.getImageAdvanced().setWidth("50%");
				upp.getImageAdvanced().setHeight("50%");
				upp.getImageAdvanced().markAsDirty();
			}

			root.addComponent(upp);

		}

	}

	public byte[] decoder(String img) {
		byte[] bytes = Base64.getDecoder().decode(img);
		return bytes;
	}

}
