package com.nogroup.ecommerce;

import java.util.Locale;

import javax.servlet.annotation.WebServlet;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nogroup.ecommerce.business.DashboardEventBus;
import com.nogroup.ecommerce.data.daos.CategoryD;
import com.nogroup.ecommerce.data.utils.InitDb;
import com.nogroup.ecommerce.views.ccmp.cntrs.TabContainer;
import com.nogroup.ecommerce.views.workflow.home.LoginView;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.Responsive;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of an HTML page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("VTheme")
public class MyUI extends UI {
	
	private static final long serialVersionUID = 1L;
	public static TabContainer container;
	private final DashboardEventBus dashboardEventbus = new DashboardEventBus();


    @Override
    protected void init(VaadinRequest vaadinRequest) {
        
		/***
		 * Style 
		 */
    	setLocale(Locale.US);
		DashboardEventBus.register(this);
		Responsive.makeResponsive(this);
		addStyleName(ValoTheme.UI_WITH_MENU);
		setTheme("VTheme");

		// Init database.
    	InitDb.initCategories();
    	InitDb.initAdmins();
    	InitDb.initUsers();
        //setContent(layout);
        //setContent(new MainView());
    	setContent(new LoginView());	

    }
   
    
    public static DashboardEventBus getDashboardEventbus() {
		return ((MyUI) getCurrent()).dashboardEventbus;
	}

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
